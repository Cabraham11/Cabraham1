<h2> My name is Christopher Abraham! 👋 👩🏾‍💻</h2>
<p><em>A well-rounded individual with strong JavaScript abilities, specifically in React and Ruby. I have a proven track record of staying current with new technologies and have the ability to quickly adapt to new concepts and ideas. I have 3+ years of experience in designing, developing, coding, and maintaining web applications.</em></p>

<br/>

![](https://komarev.com/ghpvc/?username=Cabraham1&style=plastic&color=red&label=PROFILE+VIEWS)

<br/>

```javascript
const abrahamChristopher= {
  openTowork: true,
  lookingFor: "Front-End developer";
  pronouns: ["he", "his"],
  languagesAndTools: [JavaScript, React, Redux, HTML/CSS, Semantic UI, Bootstrap,
  Tailwind CSS],
  askMeAbout: [Football, Movies,Adeventures,],
  challenge: "I am currently working on javascript and i am learning Ruby on Rails",
  funFacts: ['i can be very quick in switching from working mode to play mode'],
  reachMe: 'christopherabraham8@gmail.com'
}
```

<br/>

## My Github stats card <img src="https://media.giphy.com/media/THICzXhqZItpoFX7aD/giphy.gif" width="40">:
| <a href="https://github.com/Cabraham1/github-readme-stats"> <img align="center" src="https://github-readme-stats.vercel.app/api?username=Cabraham1&count_private=true&show_icons=true&include_all_commits=true&theme=moltack&border_radius=10" alt="Favour's github stats" /></a> | <a href="https://github.com/Cabraham1/github-readme-stats"><img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Cabraham1&layout=compact&theme=moltack&border_radius=10&card_width=280" /></a> | 
| ------------- | ------------- |
<br/>

<p align="center"><img src="https://github-readme-streak-stats.herokuapp.com/?user=Cabraham1&theme=radical" alt="Cabraham1" /></p>
  
<h3 align="center">Let's Connect 🤝</h3>
<div align="center">
<a target="_blank"
href="https://www.linkedin.com/in/abrahamchristopher/"><img
src="https://img.shields.io/badge/-LinkedIn-0077b5?style=for-the-badge&logo=LinkedIn&logoColor=white"></img></a> <a target="_blank"
href="mailto:christopherabraham8@gmail.com"><img
src="https://img.shields.io/badge/-Gmail-D14836?style=for-the-badge&logo=Gmail&logoColor=white"></img></a> <a target="_blank"
href="https://twitter.com/_Cabraham"><img
src="https://img.shields.io/badge/-Twitter-1DA1F2?style=for-the-badge&logo=Twitter&logoColor=white"></img></a>
<div/>

<p>You can check out my portfolio <a href="https://abrahamchristopher-react-portfolio.netlify.app/">here</a><img src="https://media.giphy.com/media/cKPse5DZaptID3YAMK/giphy.gif" width="60"></p>
